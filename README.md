# Ids721week6

## Create a Lambda Function

Following Week2's guide, create a new lambda function with the following command, update the function in `main.rs`.

```sh
cargo lambda new ids721-week6 \
&& cd ids721-week6
```

## Introduce Logging into Lambda Function 

To introduce logging feature in the lambda function, we need to include `tracing-subscriber` and `tracing` crates in the `Cargo.toml` file.
1. Update the code with `tracing_subscriber::fmt()` in the `main` function
2. Log the calculation process with `info!()` in the `function_handler`
3. Build and deploy the lambda function to AWS
4. Configure an API gateway for further testing

## Enable Amazon X-Ray Active Tracing
Amazon X-Ray allows us to detect, analyze, and optimize performance issues with our functions. To enable this function,
1. Go to the lambda's overview page
2. In the `Configuration` panel, choose `Monitoring and operations tools`
3. Click `Edit` in Additional monitoring tools
4. Enable Amazon X-Ray

![X-Ray](img/xray.png)


## Check AWS CloudWatch
CloudWatch is a monitoring and observability service that provides data and actionable insights to monitor your applications, understand and respond to system-wide performance changes, optimize resource utilization, and get a unified view of operational health.

After sending several requests to the lambda function through the http API gateway, we can check the logs and traces through CloudWatch.
1. Go to the lambda's monitoring section, choose `CloudWatch Log Group`
2. Scroll down to the log stream section, select one log stream

![CloudWatch Logs](img/cwlogs.png)

3. In the log stream selected, open one message starts with INFO and check the details

![Logs](img/logs.png)

4. Go back to the homepage of CloudWatch and select `Trace` under `X-Ray traces` on the side bar

![Trace](img/traces.png)

5. Select one trace and check the details
![TraceDetails](img/tracedetails.png)
![TraceDetails2](img/tracedetails2.png)