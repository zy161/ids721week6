use lambda_http::{lambda_runtime::{self, Error}, Request, RequestExt, IntoResponse, http::StatusCode, service_fn, run};
use serde_json::json;
use tracing_subscriber::filter::{EnvFilter, LevelFilter};
use tracing::{info, error};


async fn function_handler(event: Request) -> Result<impl IntoResponse, Error> {
    // Extract query string parameters
    let query_params = event.query_string_parameters();
    let number1 = query_params.first("number1").map_or(0, |n| n.parse::<i32>().unwrap_or(0));
    let number2 = query_params.first("number2").map_or(0, |n| n.parse::<i32>().unwrap_or(0));
    let product = number1 * number2;

    // Log the calculation
    info!("Calculating product of {} and {}", number1, number2);

    Ok(lambda_http::Response::builder()
        .status(StatusCode::OK)
        .body(json!({ "product": product }).to_string())
        .expect("Failed to render response"))
}

#[tokio::main]
async fn main() -> Result<(), Error> {
    tracing_subscriber::fmt()
        .with_env_filter(EnvFilter::builder().with_default_directive(LevelFilter::INFO.into()).from_env_lossy())
        .with_target(false)
        .without_time()
        .init();

    run(service_fn(function_handler)).await
}
